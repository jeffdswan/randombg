#!/bin/bash

# Randomizes background for the guake terminal.
#
# jeffdswan@gmail.com - Oct. 25 2017

bgs=(~/.backgrounds/*)
bgs=( $(shuf -e ${bgs[@]}) )
echo ${bgs[1]}
